#ifndef COLORSCHEME_H
#define COLORSCHEME_H

// dwm Default Colors
#define col_gray1 "#222222"
#define col_gray2 "#444444"
#define col_gray3 "#bbbbbb"
#define col_gray4 "#eeeeee"
#define col_cyan  "#005577"

// Dracula
#define col_drac0  "#282a36"    // Background
#define col_drac1  "#44475a"    // Current Line
#define col_drac2  "#f8f8f2"    // Foreground
#define col_drac3  "#6272a4"    // Comment
#define col_drac4  "#8be9fd"    // Cyan
#define col_drac5  "#50fa7b"    // Green
#define col_drac6  "#ffb86c"    // Orange
#define col_drac7  "#ff79c6"    // Pink
#define col_drac8  "#bd93f9"    // Purple
#define col_drac9  "#ff5555"    // Red
#define col_drac10 "#f1fa8c"    // Yellow

// Nord
#define col_nord_polarnight0 "#2e3440"
#define col_nord_polarnight1 "#384252"
#define col_nord_polarnight2 "#434c5e"
#define col_nord_polarnight3 "#4c566a"
#define col_nord_snowstorm0  "#d8dee9"
#define col_nord_snowstorm1  "#e5e9f0"
#define col_nord_snowstorm2  "#eceff4"
#define col_nord_frost0      "#8fbcbb"
#define col_nord_frost1      "#88c0d0"
#define col_nord_frost2      "#81a1c1"
#define col_nord_frost3      "#5e81ac"
#define col_nord_aurora0     "#bf616a"
#define col_nord_aurora1     "#d08770"
#define col_nord_aurora2     "#ebcb8b"
#define col_nord_aurora3     "#a3be8c"
#define col_nord_aurora4     "#b48ead"

#endif
